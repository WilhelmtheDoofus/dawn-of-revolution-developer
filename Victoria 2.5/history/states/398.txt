
state={
	id=398
	name="STATE_398"
	resources={
		steel=16.000
	}

	history={
		owner = X67
		victory_points = {
			9535 3 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 0
			arms_factory = 0
			air_base = 0

		}
		
		add_core_of = X67
	}

	provinces={
		3514 9535 
	}
	manpower=240000
	buildings_max_level_factor=1.000
	state_category=rural
}
