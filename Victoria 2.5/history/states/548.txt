
state={
	id=548
	name="STATE_548"
	resources={
		chromium=1.000
	}

	history={
		owner = U78
		victory_points = {
			13087 8 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 0
			arms_factory = 0
			air_base = 5

		}
		add_core_of = COS
		add_core_of = U78
	}

	provinces={
		12878 13087 13092 
	}
	manpower=84000
	buildings_max_level_factor=1.000
	state_category=rural
}
