
state={
	id=696
	name="STATE_696"
	resources={
		aluminium=0.000
		oil=0.000
		tungsten=3.000
	}

	history={
	owner = UNC
		add_core_of = UNC
		victory_points = {
			11006 1 
		}
		buildings = {
			infrastructure = 0
			industrial_complex = 0
			arms_factory = 0
			air_base = 0

		}
	}

	provinces={
		1570 2116 2221 5227 10941 11006 
	}
	manpower=4000
	buildings_max_level_factor=1.000
	state_category=pastoral
}
